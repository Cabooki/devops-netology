# devops-netology
В соответствии с Terraform/.gitignore Git будет игнорировать 

- файлы crash.log, override.tf, override.tf.json, .terraformrc, terraform.rc;

- каталоги .terraform и их содержимое, независимо от уровня вложенности;

- файлы с расширениями .tfvars, .tfstate и с расширением вида .tfstate.ddd;

- файлы, названия которых заканчиваются на _override.tf и _override.tf.json

